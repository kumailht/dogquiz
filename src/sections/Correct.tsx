import * as React from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import styled from 'styled-components';

import { FancyLink } from '../components/Buttons';
import { clearQuestions } from '../actions';

type breed = string;

interface State {
  breeds: breed[],
  questions: Question[],
}

interface Question {
  breed: breed,
  imageUrl: string,
}

interface Props {
  questions: Question[],
  clearQuestions(): void;
  history: any
}

class Correct extends React.Component<Props> {
  componentDidMount() {
    const that = this;
    const gameWon = this.props.questions.length > 16;

    if (gameWon) {
      this.props.clearQuestions();
      return that.props.history.push('/win/');
    } else {
      setTimeout(function() {
        that.props.history.push('/question/');
      }, 1000)
    }
  }

  render() {
    return (
      <div>
        <Title>Correct!</Title>
        <h2>Redirecting you..</h2>
        <br /><br />
        <FancyLink to="/question/">Next</FancyLink>
      </div>
    );
  }
}

const Title = styled.h1`
  margin-top: 100px;
  font-size: 80px;
  margin-bottom: 10px;
`;

// Connect state & dispatch to props
export default withRouter(connect(
  (state: State) => ({
    questions: state.questions,
  }),
  { clearQuestions }
)(Correct));