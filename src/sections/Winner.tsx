import * as React from 'react';
import { FancyLink } from '../components/Buttons';
import styled from 'styled-components';

export default class extends React.Component {
  render() {
    return (
      <div>
        <Title>YOU WON!</Title>
        <FancyLink to="/question/">PLAY AGAIN</FancyLink>
      </div>
    );
  }
}


const Title = styled.h1`
  margin-top: 100px;
  font-size: 80px;
  margin-bottom: 100px;
`;