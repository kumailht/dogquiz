import * as React from 'react';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import styled from 'styled-components';

import { FancyLink } from '../components/Buttons';
import { clearQuestions } from '../actions';

interface Props {
  clearQuestions(): void,
}

class Start extends React.Component<Props> {
  componentDidMount() {
    this.props.clearQuestions();
  }

  render() {
    return (
      <>
        <Header>
          <Title data-shadow='Dog Quiz!'>Dog Quiz!</Title>
          <Credit>Credit: <a href="https://codepen.io/carpenumidium/">Carpe numidium</a></Credit>
        </Header>
        <Footer><FancyLink to="/question">start</FancyLink></Footer>
      </>
    );
  }
}

const Footer = styled.div`
  text-align: center;
  margin: 100px 0 200px;
`;

const Header = styled.div`
  margin-top: 200px;
  position: relative;
`;

// Credit: Carpe numidium
// https://codepen.io/carpenumidium/pen/hHjEJ
const Title = styled.h1`
  color: white;
  font-family: 'Righteous', serif;
  margin: 0;
  font-size: 12em; 
  text-shadow: .03em .03em 0 #FECA54;
  position: relative;

  &:after {
    content: attr(data-shadow);
    position: absolute;
    top: .06em; left: .06em;
    z-index: -1;
    text-shadow: none;
    background-image:
      linear-gradient(
        45deg,
        transparent 45%,
        hsla(48,20%,90%,1) 45%,
        hsla(48,20%,90%,1) 55%,
        transparent 0
        );
    background-size: .05em .05em;
    -webkit-background-clip: text;
    -webkit-text-fill-color: transparent;
  
    animation: shad-anim 15s linear infinite;
  }
  
  @keyframes shad-anim {
    0% {background-position: 0 0}
    0% {background-position: 100% -100%}
  }
`;


const Credit = styled.div`
  color: white;
  position: absolute;
  bottom: 0;
  right: 0;
  a {
    color: white;
  }
`;

// Connect state & dispatch to props
export default withRouter(connect(
  state => ({}),
  { clearQuestions }
)(Start));