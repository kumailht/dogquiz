import * as React from 'react';
import { FancyLink } from '../components/Buttons';
import styled from 'styled-components';

export default class Start extends React.Component {
  render() {
    return (
      <div>
        <Title>Game Over :-(</Title>
        <FancyLink to="/question/">TRY AGAIN</FancyLink>
      </div>
    );
  }
}


const Title = styled.h1`
  margin-top: 100px;
  font-size: 80px;
  margin-bottom: 100px;
`;