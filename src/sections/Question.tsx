import * as React from 'react';
import styled from 'styled-components';
import { withRouter } from "react-router-dom";
import { connect } from 'react-redux';
import { askQuestion, fetchBreeds, fetchQuestion, markCorrect, clearQuestions } from '../actions';
import { randomKey, shuffle } from '../utils';
import { FancyButton } from '../components/Buttons';
import Loading from '../components/Loading';

type breed = string;

interface State {
  breeds: breed[],
  questions: Question[],
}

interface Question {
  breed: breed,
  imageUrl: string,
}

interface Answer {
  correct: boolean,
  breed: breed
}

interface Props {
  breeds: breed[],
  questions: Question[],
  fetchBreeds(): void,
  askQuestion(breeds: breed[]): void,
  markCorrect(breed: breed): void;
  clearQuestions(): void;
  history: any
}

class Question extends React.Component<Props> {
  componentDidMount() {
    if (!this.props.breeds) {
      this.props.fetchBreeds();
    } else {
      this.props.askQuestion(this.props.breeds);
    }
  }

  randomBreeds(items: number, exclude: breed) {
    const allBreeds = this.props.breeds;
    const randomBreeds = [];
    
    while (randomBreeds.length < items) {
      const breed = randomKey(allBreeds);
      if (breed === exclude) continue;
      randomBreeds.push(breed);
    }
  
    return randomBreeds;
  }

  handleAnswer(correct: boolean, breed: string) {
    if (correct) {
      this.props.markCorrect(breed);
      this.props.askQuestion(this.props.breeds);
      this.props.history.push('/correct/');
    } else {
      this.props.clearQuestions();
      this.props.history.push('/over/');
    }
  }

  render() {
    if (!this.props.questions.length) {
      return <Loading />
    }

    const question = this.props.questions[this.props.questions.length - 1];
    const wrongAnswers = this.randomBreeds(3, question.breed).map(breed => ({ breed, correct: false }));
    const correctAnswer = [{ breed: question.breed, correct: true }];
    const answers = shuffle([...wrongAnswers, ...correctAnswer]);

    return (
      <>
        <Title>Which breed is this dog?</Title>
        <Wrapper>
          <Dog src={question.imageUrl} width="300px" />

          <Answers>
            {answers.map(
              (ans: Answer, ans_i: number) => 
                <FancyButton key={ans_i} onClick={() => this.handleAnswer(ans.correct, ans.breed)}>
                  {ans.breed}
                </FancyButton>)}
          </Answers>
        </Wrapper>
      </>
    );
  }
}

const Answers = styled.div`
  button {
    margin-right: 10px;
    margin-bottom: 10px;
  }
`;

const Wrapper = styled.div`
  background: white;
  width: 100%;
  padding: 50px;
  margin: 50px 0;
  overflow: hidden;
`;

const Title = styled.h1`
  margin-top: 100px;
  font-size: 40px;
`;

const Dog = styled.img`
 float: right;
`;

// Connect state & dispatch to props
export default withRouter(connect(
  (state: State) => ({
    breeds: state.breeds,
    questions: state.questions,
  }),
  { askQuestion, fetchBreeds, fetchQuestion, markCorrect, clearQuestions }
)(Question));