import { combineReducers, createStore, applyMiddleware } from 'redux';
import logger from 'redux-logger'
import thunk from 'redux-thunk';

import { breeds, questionsReducer } from '../reducers';

export const reducers = combineReducers({ breeds, questions: questionsReducer });
export const store = createStore(reducers, {}, applyMiddleware(thunk, logger));