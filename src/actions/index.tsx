import { randomKey } from '../utils';

import { FETCH_BREEDS_SUCCESS, FETCH_QUESTION, UPDATE_QUESTIONS, MARK_CORRECT, CLEAR_QUESTIONS} from '../constants'

// actions.js
export function markCorrect(breed: string) {
  return { type: MARK_CORRECT, breed }
}

export function fetchBreeds() {
  return async function(dispatch) {
    const breedsStream = await fetch('https://dog.ceo/api/breeds/list/all');
    const breeds = await breedsStream.json();

    dispatch({ type: FETCH_BREEDS_SUCCESS, payload: breeds.message })
    dispatch(askQuestion(breeds.message))
  };
};

export function askQuestion(breeds) {
  return async function(dispatch) {
    const breed = randomKey(breeds);
    fetch(`https://dog.ceo/api/breed/${breed}/images/random`)
      .then(res => res.json())
      .then(data => {
        const url = data.message;
        dispatch({ type: UPDATE_QUESTIONS, question: {
          breed,
          imageUrl: url,
          correct: null,
        }})
      })
  };
};

export const fetchQuestion = (breeds) => ({ type: FETCH_QUESTION, payload: { breeds } });
export const clearQuestions = () => ({ type: CLEAR_QUESTIONS });
