import update from 'react-addons-update';
import { FETCH_BREEDS, FETCH_BREEDS_SUCCESS, FETCH_QUESTION, UPDATE_QUESTIONS, MARK_CORRECT, CLEAR_QUESTIONS} from '../constants'

export function breeds(state=null, action) {
  switch (action.type) {
    case FETCH_BREEDS:
      return state;
    case FETCH_BREEDS_SUCCESS:
      return action.payload ;
    default:
      return state;
  }
}

export function questionsReducer(questions=[], action) {
  switch (action.type) {
    case FETCH_QUESTION:
      return questions;
    case UPDATE_QUESTIONS:
      return [...questions, action.question];
    case MARK_CORRECT:
      return update(questions, {
        [questions.length - 1]: {
          correct: { $set: true } 
        }
      })
    case CLEAR_QUESTIONS:
      return [];
    default:
      return questions;
  }
}
