import { Link } from "react-router-dom";
import styled from 'styled-components';

const Fancy = (component) => component`
  color: black;
  font-size: 24px;
  background: white;
  padding: 20px 50px;
  text-transform: capitalize;
  text-decoration: none;
  font-weight: 600;
  border: 5px solid #222;
  &:hover {
    background: black;
    color: white;
    cursor: pointer;
  }
`;

export const FancyLink = Fancy(styled(Link));
export const FancyButton = Fancy(styled.button)