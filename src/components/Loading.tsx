import * as React from 'react';
import styled from 'styled-components';


function Loading() {
  return (
    <Wrap>
      Loading...
    </Wrap>
  )
}

const Wrap = styled.h1`
  width: 100%;
  height: 100%;
  text-align: center;
  margin: 300px 0;
`;

export default Loading;