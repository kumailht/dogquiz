import * as React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

import { Provider } from 'react-redux';
import { store } from './store';

import Start from './sections/Start';
import Question from './sections/Question';
import Correct from './sections/Correct';
import GameOver from './sections/GameOver';
import Winner from './sections/Winner';

ReactDOM.render(
  <Provider store={store}>
    <Router>
      <Switch>
        <Route path="/" component={Start} exact />
        <Route path="/question" component={Question} exact />
        <Route path="/correct" component={Correct} exact />
        <Route path="/over" component={GameOver} exact />
        <Route path="/win" component={Winner} exact />
        <Redirect to="/" />
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
